#include <stdlib.h>
#include <iostream>
using namespace std;

class Lista
{
	public:
	typedef struct s_nodo
	{
		int valore;
		struct s_nodo *next;
	}nodo;
	
	//dichiaro funzioni e puntatori
	void CreaNodo();
	void CreaLista();
	void Visualizza();
	void Ordina();
	
	nodo *testa;
	nodo *nuovo;
	nodo* save;
	nodo *temp;
	nodo *max;
	nodo *p;

//Inizializzo puntatori a NULL
Lista()
{
	testa = NULL;
	nuovo = NULL;
	save=NULL;
	temp=NULL;
	max=NULL;
	p = NULL;
}

};


void Lista::CreaNodo()
{
	int n;
	nuovo=(nodo*)malloc(sizeof(nodo));
	n=rand() % 10 + 1;
	nuovo->valore=n;
	nuovo->next=NULL;
}


void Lista::CreaLista() //creo lista di n nodi con valori casuali(da 1 a 10)
{
	int a;
	cout<<"Inserisci numero nodi della lista: ";
	cin>>a;
	if (a>0)
	{
	while (a!=0)
	{
		Lista::CreaNodo();
		if (testa!=NULL)
		{
		nuovo->next=testa;
		}
		testa=nuovo;
		--a;
	}
	}
}

void Lista::Visualizza()
{
	int a=1;
	if (testa!=NULL)
	{
		p=testa;
		while(p!=NULL)
		{
			cout<<"Nodo "<<a<<" --> "<<p->valore<<endl;
			p=p->next;
			++a;
		}
	}
}
